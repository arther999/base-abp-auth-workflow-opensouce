import crud from './common/crud'
import { getAllStationCategorySelection, getStationPictures, deleteStationPicture } from '@/api/stationCategory'

const stationCategory = {
  namespaced: true,
  state: {
    ...crud.state,

    propertyTemplates: []
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions,

    getAllStationCategorySelection(state) {
      return new Promise((resolve, reject) => {
        getAllStationCategorySelection()
          .then(response => {
            resolve(response.data.result.items)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getStationPictures({ state }, input) {
      return new Promise((resolve, reject) => {
        getStationPictures(input)
          .then(resp => {
            resolve(resp.data.result.items)
          })
          .catch(error => reject(error))
      })
    },

    deleteStationPicture({ state }, input) {
      return new Promise((resolve, reject) => {
        deleteStationPicture(input)
          .then(resp => {
            resolve(resp.data.result)
          })
          .catch(error => reject(error))
      })
    }
  }
}

export default stationCategory
