﻿// import Vue from 'vue'
import Vuex from 'vuex'

import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import permission from './modules/permission'
import user from './modules/user'
import profile from './modules/profile'
import session from './modules/session'
import account from './modules/account'
import tenant from './modules/tenant'
import role from './modules/role'
import organizationUnit from './modules/organizationUnit'
import notification from './modules/notification'
import auditlogs from './modules/auditLogsAndEntityChanges/auditlogs'
import entityChanges from './modules/auditLogsAndEntityChanges/entityChanges'
import stationCategory from './modules/stationCategory'

import commonTask from './modules/commonTask'
import advanceTask from './modules/advanceTask'
import flowScheme from './modules/flowScheme'
import noGenerate from './modules/noGenerate'
import leaveReqForm from './modules/leaveReqForm'
import simpleTask from './modules/simpleTask'
// {#insert import code#}

// Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    permission,
    user,
    profile,
    session,
    account,
    tenant,
    role,
    organizationUnit,
    notification,
    auditlogs,
    entityChanges,
    stationCategory,
    commonTask,
    advanceTask,
    flowScheme,
    noGenerate,
    leaveReqForm,
    simpleTask// {#insert module code#}
  },
  getters
})

export default store
