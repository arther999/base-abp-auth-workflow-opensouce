import Cookies from 'js-cookie'
import appconst from '@/vendor/abp/appconst'

const TokenKey = 'Admin-Token'

export function getToken() {
  return window.abp && window.abp.session && window.abp.session.userId
  // return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  abp.auth.clearToken()
  abp.utils.deleteCookie(appconst.authorization.encrptedAuthTokenName)
  // return Cookies.remove(TokenKey)
}
