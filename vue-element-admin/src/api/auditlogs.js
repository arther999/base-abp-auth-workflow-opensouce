import request from '@/utils/request'

export function getAuditLogs(data) {
  return request({
    url: '/api/services/app/AuditLog/GetAuditLogs',
    method: 'get',
    params: data
  })
}

export function getAuditLogsToExcel(data) {
  return request({
    url: '/api/services/app/AuditLog/GetAuditLogsToExcel',
    method: 'get',
    params: data
  })
}

export function getEntityHistoryObjectTypes() {
  return request({
    url: '/api/services/app/AuditLog/GetEntityHistoryObjectTypes',
    method: 'get'
  })
}

export function getEntityChanges(data) {
  return request({
    url: '/api/services/app/AuditLog/GetEntityChanges',
    method: 'get',
    params: data
  })
}

export function getEntityChangesToExcel(data) {
  return request({
    url: '/api/services/app/AuditLog/GetEntityChangesToExcel',
    method: 'get',
    params: data
  })
}

export function getEntityPropertyChanges(data) {
  return request({
    url: '/api/services/app/AuditLog/GetEntityPropertyChanges',
    method: 'get',
    params: data
  })
}
