﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;

namespace MonitorCenter.ETag
{
    public interface IETagStoreManger : ISingletonDependency
    {
        Task ClearETagAsync();

        Task RemoveETagAsync(ETagCacheKey key);

        Task RemoveUserDefaultETagAsync(int? tenantId, long? userId);

        Task<EtagCacheValue> GetETagAsync(ETagCacheKey key, Func<Task<EtagCacheValue>> facotory);
    }
}
